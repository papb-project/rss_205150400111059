package id.ub.rss_205150400111059;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class LoadRess extends AsyncTask <String, String, String>{

    TextView tv1;

    public LoadRess(TextView tv1) {
        this.tv1 = tv1;
    }

    @Override
    protected String doInBackground(String... strings) {
        String url = strings[0];
        Log.d("RSSPARSER", "Start rss parser");
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            String xml = response.body().string();
            RssParser parser = new RssParser();
            List<String> listJudul = parser.parseRss(xml);

            String res = "";
            for (String judul : listJudul){
                res = res + judul + '\n';
                res+="----------------------------------\n";
                Item satu = new Item(res);
            }
            return res;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        tv1.setText(s);
//        MainActivity tes = new MainActivity();
//        tes.list.add(new Item(tv1));
    }
}

