package id.ub.rss_205150400111059;

import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class RssParser {
    private static String TAG_ITEM = "item";
    private static String TAG_TITLE = "title";
    private static String TAG_CHANNEL = "channel";

    public List<String> parseRss(String xml) throws ParserConfigurationException, IOException, SAXException {
        List<String> list = new ArrayList<>();
        DocumentBuilderFactory builder = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = builder.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xml));
        Document doc = db.parse(is);
        NodeList nodeList = doc.getElementsByTagName(TAG_CHANNEL);
        Element e = (Element) nodeList.item(0);

        NodeList items = e.getElementsByTagName(TAG_ITEM);
        for (int i = 0; i < items.getLength(); i++) {
            Element e1 = (Element) items.item(i);
            NodeList n = e1.getElementsByTagName(TAG_TITLE);
            Node ne = n.item(0);
            Node child = ne.getFirstChild();
            String judul = child.getNodeValue();
            Log.d("RSSPARSER", judul);
            list.add(judul);
        }
        return list;
    }
}
