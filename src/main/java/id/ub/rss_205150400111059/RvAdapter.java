package id.ub.rss_205150400111059;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RvAdapter extends RecyclerView.Adapter<RvAdapter.ViewHolder> {
    ArrayList list;
    LayoutInflater inflater;
    Context context;

    public RvAdapter(ArrayList list, Context context){
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.recycle, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RvAdapter.ViewHolder holder, int position) {
        Item i = (Item) list.get(position);
        holder.tv1.setText(i.tv1);

    }

    @Override
    public int getItemCount() {
        Log.d("ADAPTER", "jumlah baris= "+list.size());
        return list.size();
    }

    class Baru extends AppCompatActivity{

    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView tv1;
        String url = "https://medium.com/feed/tag/programming";

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv1 = itemView.findViewById(R.id.tv1);
            LoadRess loadRss = new LoadRess(tv1);
            loadRss.execute(url);
        }
    }
}
